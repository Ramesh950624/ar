﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemController : MonoBehaviour {
    public Animator anim;
    public AudioSource[] _audio;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        anim = GetComponent<Animator>();
        _audio = GetComponent<AudioSource[]>();
	}
    public void GolemActions(string ActionCommands)
    {
      switch(ActionCommands)
        {
            case "jump":
                anim.Play("jump",-1,0f);
                _audio[0].Play();
                break;
            case "rage":
                anim.Play("rage", -1, 0f);
                _audio[1].Play();
                break;
            default:
                anim.Play("idle", -1, 0f);
                break;
        }
    }
}
